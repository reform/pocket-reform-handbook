#!/bin/bash

for f in motherboard keyboard-kailh-ortho charger
do
    echo =====================================================
    echo $f
    echo =====================================================
    ruby source/scripts/cleanup-bom.rb ~/src/pref/pocket-reform/pocket-reform-$f/pocket-reform-$f.csv source/_static/bom/pocket-reform-$f.csv
    echo =====================================================
done
