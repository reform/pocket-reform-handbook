#!/bin/sh

DIR=$(pwd)

cd source/_inkscape
for target in *.svg
do
        targeta="../_static/illustrations/${target%.svg}.png"
        echo "Creating: $targeta"
        inkscape -o "$targeta" --export-area-page -w 1280 "$target"
done
cd $DIR
./make-pngs.sh source/_static/illustrations
./make-pngs.sh source/_static/illustrations/renders

# FIXME
for f in tina reform-v4-sway reform-v4-wayfire weee renders/pocket-reform-render-top-half renders/pocket-reform-render-bottom-half renders/pocket-reform-render-panel-display-back renders/pocket-reform-render-panel-keyboard-frame renders/pocket-reform-render-panel-keyboard-back renders/pocket-reform-render-oled-module
do
    cp source/_static/illustrations/$f.png source/_static/illustrations/$f-icon.png
done

# FIXME
sed -i 's/+=========================+================================================================================+/+-------------------------+--------------------------------------------------------------------------------+/g' source/software.rst

make clean
make html

# remove alabaster css
echo "" > build/html/_static/alabaster.css

# remove printer logo from web version
mv build/html/credits.html build/html/temp.html
grep -v pinguin <build/html/temp.html >build/html/credits.html
rm build/html/temp.html

# remove image sources
rm -rf build/html/_static/illustrations
