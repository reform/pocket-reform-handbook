#!/bin/bash

rm source/_static/illustrations/*.eps
DIR=$(pwd)

cd source/_inkscape
for target in *.svg
do
	targeta="../_static/illustrations/${target%.svg}.eps"
	echo "Creating: $targeta"
	inkscape -o "$targeta" "$target"
done
cd $DIR

cd source/_static/illustrations/renders
for target in *.svg
do
	targeta="${target%.svg}.eps"
	echo "Creating: $targeta"
	inkscape -o "$targeta" "$target"
done
cd $DIR

