#!/bin/bash

shopt -s extglob nullglob

cd "$1"

for i in ./*.@(eps|svg)
do
	target="${i/\.eps/.png}"
	target="${i/\.svg/.png}"
	echo "$i -> $target"
	if [[ ! -e $target ]]; then
		inkscape -o $target --export-area-page -w 1280 $i
	fi
done
