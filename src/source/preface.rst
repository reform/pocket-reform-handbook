.. role:: raw-latex(raw)
   :format: latex

Preface to the First Edition
++++++++++++++++++++++++++++

It's quite something to get the chance to release not only one, but two open hardware laptops into the world. After two roller coaster years of development, we've finally started shipping MNT Pocket Reforms (not yet two weeks before I'm writing this preface, on a Pocket Reform, of course). Pocket Reform is a reaction to years of scrolling on glass, longing for physical buttons to press, looking for escape hatches from the digital gated communities of corporations. It is about the desire to have a small, yet unconstrained computer. In the words of El-P: "Let's boil it down to the simplest." This computer is in your hands against all odds, again (swimming upstream, MNT Reform was already improbable). There are three groups who made this happen: on the one hand, my small but relentless team of people who always did their best, and did not give up until it was finished. On the other hand, you, the owners of and contributors to MNT (Pocket) Reforms, who we have to thank for your endless support. And on the third hand, Crowd Supply, for believing in us and trusting that we'll actually deliver.

But enough of the sentimentalities---enjoy this computer, take it to places, take it apart, connect, customize, hack, and most importantly, have fun.

---Lukas "minute" Hartmann, Berlin, 2024-05-31

:raw-latex:`\mainmatter`
