.. role:: raw-latex(raw)
   :format: latex

:raw-latex:`\clearpage`
:raw-latex:`\ifodd\value{page}\hbox{}\newpage\fi`
:raw-latex:`\includepdf[noautoscale]{_static/illustrations/pref-a5-cafe.pdf}`

Graphical Desktops
==================

Sway and Wayfire
----------------

MNT Pocket Reform ships with two graphical environments called "desktops". The Debian distribution, which the system on the eMMC is based on, has a number of additional desktops in its package manager (see section "Alternative Desktops" at the end of this chapter).

1. **Sway** emphasizes the concept of "tiling". This means that normally, windows don't overlap, but instead the screen space is automatically divided to make space for new windows. This desktop consumes minimal system resources, but relies heavily on keyboard shortcuts, which makes it harder to learn.

2. **Wayfire** features classic overlapping windows, a point-and-click driven interface, and a modern look. In Wayfire, you can drag windows to the corners or edges of the screen and they will be snapped. They also have minimize, maximize, and close buttons. If you're accustomed to GNOME, Windows, or macOS desktops, you'll find Wayfire quite intuitive.

You initially choose between Sway and Wayfire in the MNT Pocket Reform Setup Wizard. This choice will be reflected as the default on the login screen, but you can change it by pressing *F3*.

:raw-latex:`\newpage`

Sway and Wayfire look and work differently, but they share the following functionalities and keyboard shortcuts with *SUPER* [#]_:

.. [#] *SUPER* is the key with the MNT Research logo next to the *HYPER* key.

================== ====================================
Key Combinations   Function
================== ====================================
*SUPER+ENTER*      Open a terminal
*SUPER+ESC*        Close active window
*SUPER+D*          Find and launch programs
*SUPER+SHIFT+S*    Take screenshot (in ~/Pictures)
*SUPER+SHIFT+X*    Take screenshot of an area
*SUPER+1...*       Go to workspace 1--9
*SUPER+SHIFT+1...* Move active window to workspace 1--9
================== ====================================

Launching Applications
++++++++++++++++++++++

Pocket Reform's Sway and Wayfire desktops include the ``wofi`` launcher, a popup menu for launching an application by typing a part of its name. Press *SUPER+D* to open the menu. Over time, ``wofi`` will remember the applications you regularly launch and will sort them by frequency of use.

Top Bar
+++++++

Both desktops come with an information bar at the top of the screen provided by the ``waybar`` package. The bar is divided into the following sections:

================================ ==================================
Section                          Action on click
================================ ==================================
MNT Research logo                Open the launcher
Icons of running applications    Switch to application
Workspace numbers (only on Sway) Switch to workspace
Tray icons                       Settings for network,

                                 volume, etc.
Clock                            Toggle date/clock
================================ ==================================

You can completely customize the top bar by editing the config file ``~/.config/waybar/config`` and the CSS-based styling file ``~/.config/waybar/style.css``.

``man waybar`` explains the available configuration options.

Workspaces
++++++++++

If you have a lot of windows open, they might not all fit on the screen at once. For this purpose, multiple set of windows can be arranged in workspaces.

You can change your active workspace with the number keys, for example:

=============== =================================
Shortcut        Function
=============== =================================
*SUPER+2*       Go to workspace 2
*SUPER+1*       Go back to workspace 1
*SUPER+SHIFT+5* Move active window to workspace 5
=============== =================================

You can open different spaces for different programs. For example, you might want to put your code-editing programs in workspace 1, a web browser in workspace 2, and some instant messaging programs in workspace 3.

Display Brightness
++++++++++++++++++

You can set the display's brightness using the ``brightnessctl`` command or, more conveniently, use one of these keyboard shortcuts:

========== ===========================
Shortcut   Function
========== ===========================
*HYPER+F1* Decrease display brightness
*HYPER+F2* Increase display brightness
========== ===========================

Network and Wi-Fi
+++++++++++++++++

You can connect to Wi-Fi networks and configure Ethernet as well as VPN connections by clicking the network icon in the top bar.

Bluetooth
+++++++++

If your system has Bluetooth, you'll see a Bluetooth icon in the tray area of the top bar. This icon is powered by the ``blueman`` package behind the scenes and clicking on it with the left mouse button will open the *Bluetooth Devices* window, where you can search for nearby devices to connect to. Right clicking on a Bluetooth device will give you the options to connect, pair, or get more information. You can also right-click on the Bluetooth icon in the top bar itself to open a context menu with more options, like toggling the Bluetooth visibility of your MNT Pocket Reform, sending files or reestablishing recent connections.

Note that any Bluetooth audio devices, when connected, will appear in *Volume Control* (started by launching it from the speaker icon in the top bar or manually from the launcher). Here you can choose them as the default output or input device and adjust the volume.

Sway Specifics
--------------

.. image:: _static/illustrations/reform-v4-sway-icon.png

Tiling
++++++

You can start a new terminal window by using the shortcut *SUPER+ENTER*. When you press *SUPER+ENTER* multiple times to open several terminals, you'll notice that your currently open windows will be resized to accommodate for the new window. You can switch between these windows by holding the *SUPER* key and pressing the cursor (arrow) keys in the desired direction.

If you keep adding windows, they will continuously shrink horizontally, but if you would rather have a window split vertically, you can. Use these shortcuts for deciding:

========= =========================
Shortcut  Function
========= =========================
*SUPER+H* Split window horizontally
*SUPER+V* Split window vertically
========= =========================

Note that the window is not split instantaneously. You're just telling Sway "The next time I create a window, put it below/beside my current window."

You may also use *SUPER+W* to tell Sway to use tabs. You can switch your tab using the same shortcuts for switching between windows. You can end this function by pressing *SUPER+E*.

You can use *SUPER+ESC* to close the currently selected window.

Sway Config File
++++++++++++++++

You can tailor Sway's behavior and keyboard shortcuts by editing the file ``~/.config/sway/config`` or one of the included files in the ``~/.config/sway/config.d`` directory.

All configuration options are documented in the manual page that you can access by typing ``man 5 sway`` in a terminal. More information is also available in the Sway Wiki: `<https://github.com/swaywm/sway/wiki>`_

Some of the most important configuration options are explained in the following sections.

Keyboard Layout
+++++++++++++++

The keyboard layout is normally configured by the MNT Pocket Reform Setup Wizard. Should you want to configure it manually, you can edit the ``~/.config/sway/config.d/input`` file. For example, the following snippet will change the layout to the EU layout for any connected keyboard:

.. code-block:: none

 input * {
   xkb_layout eu
   xkb_options lv3:ralt_switch
 }

Trackball/Trackpad Speed
++++++++++++++++++++++++

A common thing that people want to tune to their particular taste is the speed of the trackball (or trackpad) on MNT Pocket Reform. For Sway, you could add an ``input`` section (or extend the existing one) to the ``~/.config/sway/config.d/input`` file as such:

.. code-block:: none

 input * {
   accel_profile adaptive
   pointer_accel 1.0
 }

Where ``pointer_accel`` can be a number between -1.0 and 1.0.

You can find out about all of the details of Sway's input configuration by opening its manual page using ``man sway-input``.

External Displays
+++++++++++++++++

Some Processor Modules allow you to connect an external display on the HDMI port, but not all of them can drive both the internal and the external display at the same time. If your module supports dual display, plugging in an external monitor should just work. If you want to configure the placement and resolution of this monitor, take a look at ``man sway-output`` and create an ``output HDMI-A-1`` section in your Sway config file. Other helpful tools that you can install for these purposes are ``kanshi`` and ``wlr-randr``.

You can also use ``swaymsg`` to control your displays on the fly or when pressing certain key combinations. The following example script will switch from internal to external display and force a standard 1080p mode:

.. code-block:: none

 #!/bin/sh
 swaymsg output eDP-1 disable
 swaymsg output HDMI-A-1 enable
 swaymsg output HDMI-A-1 modeline 148.50 1920 2448 2492\
                                  2640 1080 1084 1089\
                                  1125 +hsync +vsync

Wayfire Specifics
-----------------

.. image:: _static/illustrations/reform-v4-wayfire-icon.png

Wayfire supports a range of keyboard shortcuts to speed up working with the desktop:

============== ============================
Shortcut       Function
============== ============================
*SUPER+← or →* Tile window to left or right
*SUPER+↑ or ↓* (Un)maximize window
*SUPER+TAB*    Overview of all windows
*ALT+TAB*      Cycle through windows
============== ============================

Wayfire Config File
++++++++++++++++++++

You can customize Wayfire in depth by editing the file:

.. code-block:: none

   ~/.config/wayfire.ini

Some common configuration options are explained in the following sections.

Keyboard Layout
+++++++++++++++

Wayfire's keyboard layout is normally configured by the MNT Pocket Reform Setup Wizard. Should you want to configure it manually, you can edit the file ``~/.config/wayfire.ini``. Look for the line ``xkb_layout`` in the section ``[input]``. For example, to change it to the "EurKEY" layout, change the value after the ``=`` sign to ``eu``.

Trackball/Trackpad Speed
++++++++++++++++++++++++

To change the speed of the trackball (or trackpad) cursor under Wayfire, look for the lines in the ``[input]`` section of ``~/.config/wayfire.ini`` that start with ``touchpad``. To achieve a high acceleration speed, set the ``touchpad_accel_profile`` to ``adaptive`` and adjust the ``touchpad_cursor_speed`` value to ``1.0`` (it can be a number between ``-1.0`` and ``1.0``).

You can learn all the details of Wayfire's configuration in its wiki: `<https://github.com/WayfireWM/wayfire/wiki>`_

Alternative Desktops
--------------------

On MNT Pocket Reform, you are not restricted to Sway or Wayfire---you can also use other desktops.

You can choose an established desktop such as KDE Plasma and GNOME, or you can go down the rabbit hole of arcane desktops and discover a hidden gem this way.

We don't ship either of the big desktops as they require a lot of resources and we want to keep our system minimalistic, but here is how you can install them yourself:

GNOME
  Command: ``sudo apt install gnome``

KDE Plasma
  Command: ``sudo apt install kde-plasma-desktop``
