.. role:: raw-latex(raw)
   :format: latex

Safety & Recycling
==================

Hazards
-------

.. image:: _static/illustrations/safety-pocket-reform-drawing.png

:raw-latex:`\newpage`

Before you get started with your MNT Pocket Reform, please read these safety instructions carefully to prevent harm to yourself and your environment.

**Electrical Shock and Fire Hazard:** Please be extra careful while and after opening the case of the device. MNT Pocket Reform uses 2 Li-ion pouches. Do not touch anything connected to the batteries or the charger board with metal tools. Refer to the Hardware chapter for instructions on how to replace the batteries safely.

**Pinch Point Hazard:** The case closes with strong magnets and the top half has sharp edges. Hazard of fingers pinching! Keep out of reach of children!

**Before servicing anything on the inside, make sure that the wall power is unplugged and remove the battery cells.**

**Damage to Hearing:** The headphone output of MNT Pocket Reform can be forced to extreme volume which may damage your hearing if you are not careful. Please make sure to set the volume to 30% or less before connecting headphones to MNT Pocket Reform, and then adjust the volume to a comfortable level.

Recycling
---------

.. image:: _static/illustrations/weee-icon.png
  :width: 10%

Don't throw any MNT Pocket Reform parts in the trash! Batteries and electronics contain materials that are harmful to the environment if not properly disposed of.

You can mail any of the parts back to MNT Research, and we will recycle them for you. Alternatively, you can recycle batteries at a local battery collection facility and dispose of electronics and cables at a local e-waste facility.
