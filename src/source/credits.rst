Credits
=======

.. role:: raw-latex(raw)
   :format: latex

The MNT Pocket Reform Operator Handbook, Corrected First Edition. Berlin, July 2024.
Written by Lukas F. Hartmann and Anri Paul Hennies. Published by MNT Research GmbH.

:Concept, Electronics, Software: Lukas F. Hartmann

:Industrial Design: Ana Beatriz Albertini Dantas

:Quality & Assembly, Sleeve Design: Greta Melnik

:Assembly & Testing: Liaizon Wakest

:Handbook Design: 100rabbits, Lukas F. Hartmann

:3D Illustration: Lukas F. Hartmann, Philipp Brömme

:2D Illustration: Anri Paul Hennies

:Tina Pixel Icon: Greta Melnik

:Accounting: Christian 'plomlompom' Heller

:Debian Integration & Tooling: Johannes Schauer Marin Rodrigues

:Contributions & Thanks: Pooja Channaveerappa, Katharina Withelm, Brendan Nystedt, Birute Mazeikaite, timonsku, Dirk Eibach, F. Schneider, Marek Vasut, Zoé & Elen (Fully Automated)

:Therapy Dog: Tina

:Crowdfunding Partner: Crowd Supply

:Additional Funding: NLNet

:Printed Circuit Boards: PCBWay, JLCPCB

:Milling: JPR

:Inter Font: @rsms

:Lab Tools: Rigol DS1054Z, Fluke 117, Weller WD-1, Puhui T-962C, AmScope SM-4T, BCN3D Sigma R17, Formlabs Form2, Epilog Mini 18, Prusa i3 MK3S+

:Software: Debian GNU/Linux, KiCad, Inkscape, GIMP, Emacs, Vim, GitLab, Blender, FreeCAD, OpenSCAD, Fusion, Sphinx, Pandoc, XeTeX, Scribus

.. image:: _static/illustrations/tina-icon.png
   :width: 30%

| © 2018-2024 MNT Research GmbH, Fehlerstr. 8, 12161 Berlin
| Managing Director: Lukas F. Hartmann
| Registergericht: Amtsgericht Charlottenburg
| Aktenzeichen: HRB 136605 B
| VAT ID: DE278908891
| WEEE: DE 33315564
| Web: https://mntre.com

MNT Pocket Reform is Open Source Hardware.

The sources for this handbook and most MNT Pocket Reform software components (check repositories for details) are licensed under GNU GPL v3 or newer. The artwork is licensed under CC BY-SA 4.0. The MNT Pocket Reform hardware design files are licensed under CERN-OHL-S 2.0. The MNT symbol is a registered trademark of MNT Research GmbH.

.. image:: _static/illustrations/pinguin-druck.png
   :width: 30%
