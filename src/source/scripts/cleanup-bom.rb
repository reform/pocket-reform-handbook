require 'csv'

infn=ARGV[0]
outfn=ARGV[1]

HDR_REF = "Reference(s)"
HDR_QTY = "Qty"
HDR_VAL = "Value"
HDR_MF = "Manufacturer"
HDR_PN = "Manufacturer_No"

if system("head -n1 #{infn} | grep Source")
  `tail -n +9 #{infn} > #{outfn}`
else
  `cp #{infn} #{outfn}`
end

data=CSV.read(outfn, :headers => true)
headers=data[0].headers
puts headers

def ref_to_row_score(x)
  matched = x.match(/^([A-Z]+)([0-9]+)/i)
  if matched
    alpha, num = matched.captures
    score = "#{alpha}#{num.rjust(5,"0")}"
    return score
  else
    return "!#{x}"
  end
end

def ref_to_num(x)
  x.split("-").last.gsub(/[^0-9]/,'').to_i
end

def merge_designators(row)
  refs = row[HDR_REF].split(", ").sort_by {|x| ref_to_num(x)}
  done = false
  while !done && refs.size>1 do
    i = 0
    loop do
      b = ref_to_num(refs[i+1])
      a = ref_to_num(refs[i])
      if b==a+1
        # merge with next
        refs[i] = [refs[i].split("-").first, refs[i+1]].join("-")
        refs.delete(refs[i+1])
        break
      end
      i += 1
      if i == refs.size-1
        done = true
        break
      end
    end
  end
  row[HDR_REF] = refs.join(" ")
end

def clean_value(row)
  v = row[HDR_VAL]
  r = row[HDR_REF][0]
  if !['R','C','L','Y'].include?(r)
    row[HDR_VAL] = ""
  elsif r == "R" && v.size>0 && !v.match("DNP") && v!="NOSTUFF"
    parts = v.split(" ")
    parts[0].gsub!("K","k")
    parts[0]+="Ω"
    row[HDR_VAL]=parts.join(" ")
  end
  row[HDR_VAL] = "" if row[HDR_VAL] == row[HDR_PN]
end

CSV.open(outfn, "w") do |out|
  out << ["Designator","Qty","Value","Brand","Part Number"]

  data.each do |row|
    merge_designators(row)
    clean_value(row)
  end

  data = data.sort_by {|y| ref_to_row_score(y[HDR_REF]) }
  data.each do |row|
    puts "--------"
    puts row

    if row[HDR_REF][0..1]!="TP" && row[HDR_REF][0..1]!="MK" && row[HDR_REF][0..2]!="GFX" && !row[HDR_PN].nil? && row[HDR_PN].size>0
      out << [row[HDR_REF], row[HDR_QTY], row[HDR_VAL], row[HDR_MF], row[HDR_PN]]
    end
  end
end
