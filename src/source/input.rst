.. role:: raw-latex(raw)
   :format: latex

:raw-latex:`\clearpage`
:raw-latex:`\ifodd\value{page}\hbox{}\newpage\fi`
:raw-latex:`\includepdf[noautoscale]{_static/illustrations/pref-a5-magic.pdf}`

Input Devices
=============

MNT Pocket Reform comes with a keyboard and a trackball preinstalled.

Keyboard
--------
.. image:: _static/illustrations/input-pocket-keyboard-special-keys.png

Pocket Reform has a mechanical ortholinear[#]_ keyboard with 60 keys.

Because many advanced users remap the traditional Caps Lock key to a different function, we swapped Caps Lock for a CTRL key. This makes the use of CTRL key combinations more ergonomic.

Another handy key is the *AGR* key, short for "Alternate Graphic [Alt Gr]". Holding this key and a key of your choice gives you the option to create international characters such as Umlauts and other special characters. If you are a non-English native speaker, we recommend the so-called "EurKEY"[#]_ keyboard layout. This layout is based on the QWERTY-US layout and can be activated in your keyboard settings. See the graphic below for characters that can be created on Pocket Reform holding the *AGR* key and the corresponding key:

.. image:: _static/illustrations/input-pocket-keyboard-eurkey.png

Lastly, MNT Pocket Reform features an additional modifier key, the *HYPER* key, in the lower left. *HYPER* provides an additional layer of key combinations. Here is the list of shortcuts you can use with *HYPER*:

===================== =====================
Shortcut              Function
===================== =====================
*HYPER+ENTER* (hold)  Turn on Pocket Reform
*HYPER+ENTER* (press) Turn on OLED Menu
*HYPER+1*             F1
*HYPER+2*             F2
*HYPER+3*             F3
*HYPER+4*             F4
*HYPER+5*             F5
*HYPER+6*             F6
*HYPER+7*             F7
*HYPER+8*             F8
*HYPER+9*             F9
*HYPER+10*            F10
*HYPER+Q*             F11
*HYPER+W*             F12
*HYPER+←*             Home
*HYPER+→*             End
*HYPER+↑*             Page Up
*HYPER+↓*             Page Down
*HYPER+O*             [
*HYPER+P*             ]
*HYPER+SHIFT+O*       {
*HYPER+SHIFT+P*       }
*HYPER+SHIFT+ESC*     ~
*HYPER+ESC*           `
===================== =====================

.. [#] Ortholinear means that all keys are aligned in a perfect grid.
.. [#] Created by Steffen Brüntjen.

OLED Menu
---------

.. image:: _static/illustrations/4-oled-menu.png

The keyboard has a built-in OLED display for interaction with the System Controller on the motherboard. You can highlight an option and scroll through the menu by using the *↑* and *↓* keys. To trigger the highlighted option, press *ENTER*. Alternatively, you can press the shortcut key that is displayed on the right hand side of each menu option. For example, to show the Battery Status, press *B* when the menu is active. To leave the menu, press *ESC*.

You can see detailed battery information including the estimated total charge percentage on the Battery Status screen reachable through the OLED menu. Each cell icon corresponds to one of the two battery cells. The upper icon represents the battery pack on the left side of the device, and the lower icon represents the battery pack on the right---assuming you look at MNT Pocket Reform when flipped on its back and the batteries closest to you.

.. image:: _static/illustrations/4-oled-battery-status.png

Trackball
---------
.. image:: _static/illustrations/input-trackball-buttons.png

The trackball works like a mouse with four buttons: middle button, left click, scroll button, and right click. Roll the ball to move the cursor. Pressing the *Middle Button* and the *Scroll Mode* button simultaneously activates *Sticky Scroll Mode*, so that you can scroll through a longer text without needing to hold down a button.
