.. role:: raw-latex(raw)
   :format: latex

Schematics
++++++++++

All of our Pocket Reform-related schematics, design files and documentation are publicly available on our GitLab instance:

`<https://source.mnt.re/reform/pocket-reform>`_

The following sections link to KiCad projects of the circuit boards that you can explore interactively.

:raw-latex:`\small`

Motherboard Schematics
======================

- `PDF: Motherboard Schematics <_static/schem/pocket-reform-motherboard.pdf>`_
- `KiCad: Motherboard Design Files <https://source.mnt.re/reform/pocket-reform/-/tree/main/pocket-reform-motherboard>`_

:raw-latex:`\includepdf[pages=-]{_static/schem/pocket-reform-motherboard.pdf}`

Motherboard Bill of Materials
=============================

.. csv-table::
   :file: _static/bom/pocket-reform-motherboard.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Keyboard Schematics
===================

- `PDF: Keyboard Schematics <_static/schem/pocket-reform-keyboard-kailh-ortho.pdf>`_
- `KiCad: Keyboard Design Files <https://source.mnt.re/reform/pocket-reform/-/tree/main/pocket-reform-keyboard-kailh-ortho>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/pocket-reform-keyboard-kailh-ortho.pdf}`
:raw-latex:`\includepdf[pages=2]{_static/schem/pocket-reform-keyboard-kailh-ortho.pdf}`
:raw-latex:`\includepdf[pages=3,angle=90]{_static/schem/pocket-reform-keyboard-kailh-ortho.pdf}`

Keyboard Bill of Materials
==========================

.. csv-table::
   :file: _static/bom/pocket-reform-keyboard-kailh-ortho.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

OLED Schematics
===============

- `PDF: OLED Schematics <_static/schem/reform2-oled.pdf>`_
- `KiCad: OLED Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-oled-pcb>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/reform2-oled.pdf}`

OLED Bill of Materials
======================

.. csv-table::
   :file: _static/bom/reform2-oled.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Trackball Sensor Schematics
===========================

- `PDF: Trackball Sensor Schematics <_static/schem/reform2-trackball-sensor.pdf>`_
- `KiCad: Trackball Sensor Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-trackball-sensor-pcb>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/reform2-trackball-sensor.pdf}`

Trackball Sensor Bill of Materials
==================================

.. csv-table::
   :file: _static/bom/reform2-trackball-sensor.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Charger Schematics
=======================

- `PDF: Motherboard Schematics <_static/schem/pocket-reform-charger.pdf>`_
- `KiCad: Keyboard Design Files <https://source.mnt.re/reform/pocket-reform/-/tree/main/pocket-reform-charger>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/pocket-reform-charger.pdf}`

Charger Bill of Materials
=========================

.. csv-table::
   :file: _static/bom/pocket-reform-charger.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Assembly Parts
==============

.. csv-table::
   :file: _static/bom/assembly-parts.csv
   :header-rows: 1
   :widths: 30,3,26,43
