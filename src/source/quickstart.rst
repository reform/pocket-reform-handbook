Quick Start
===========

Step 1: Standby Power Switch
----------------------------

The batteries are connected by default. There is a small Standby Power Switch located on the left hand side of the upper case part. Turn it on using a pointed object such as a SIM card ejector.

.. image:: _static/illustrations/hardware-left-cover.png

Step 2: Turn On
---------------

.. image:: _static/illustrations/quickstart-pocket-keyboard-turn-on.png

To turn on Pocket Reform, hold *Hyper* + *Enter* for more than 2 seconds.

Step 3: Log In
--------------
After being powered on, the main processor will boot the operating system installed on the built-in eMMC flash. The operating system's kernel will show diagnostic information as it activates all the devices in the system until finally arriving at the login prompt. On first boot, the Setup Wizard will walk you through the configuration process. You'll select your keyboard layout and your time zone, choose a desktop environment, and finally, create a user account. Once everything is set up, you can start installing software and using MNT Pocket Reform. If you are new to the Debian GNU/Linux operating system or want to learn about specifics of the system software shipped with MNT Pocket Reform, please refer to the chapters "Linux Console Basics", "Graphical Desktops", and "Software".

The following chapter will introduce you to the input devices of MNT Pocket Reform.
