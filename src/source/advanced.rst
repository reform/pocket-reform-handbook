.. role:: raw-latex(raw)
   :format: latex

:raw-latex:`\clearpage`
:raw-latex:`\ifodd\value{page}\hbox{}\newpage\fi`
:raw-latex:`\includepdf[noautoscale]{_static/illustrations/pref-a5-lab.pdf}`

Advanced Topics
===============

A computer is a complex system where hardware and software interact, and sometimes, unpredicted problems may appear. Most issues can be fixed, and in this chapter we will show you how.

This is for advanced users only. If you are unsure about what you are doing, we encourage you to get in touch with our MNT Community or our email support (``support@mntre.com``).

Troubleshooting
---------------

There are some steps you can try if MNT Pocket Reform should stop booting. As always, before working with MNT Pocket Reform internals, first disconnect the internal batteries and external power and peripherals.

Boot Issues
+++++++++++

What can you do if your MNT Pocket Reform doesn't boot?

There are several steps you can take to determine the cause.

First of all, check if it has power. If you are in luck, a cable or a connector is just loose and you can plug it back in.

If this doesn't work, take a look at the OLED screen. Does it show anything when you press the *Hyper* key together with *Enter*? If not, this could have several reasons: the OLED module or the two cables of the keyboard may not be fully connected. It is also possible that the keyboard or the motherboard are defective.

If the keyboard is working, but your MNT Pocket Reform doesn't boot, it could be that the bootloader on the eMMC flash encountered an error. If you have another computer at hand, install the latest System Image to a microSD card. Refer to the chapter "Software" and the section "Flashing the System Image" in this handbook.

Insert the freshly flashed microSD card and try to boot MNT Pocket Reform. If this isn't successful, the motherboard or the Processor Module or even the display could be defective. If you have another Processor Module and knowledge of how to swap it, you could try that. We recommend getting in touch with our customer support as you have a 2-year warranty on your device. Warranty repairs are free within this period. Please refer to our FAQ (`<https://mntre.com/faq.html>`_) for more details.

Serial Console
++++++++++++++

If you have good Linux knowledge and surmise the boot issue is not hardware related, you can try fixing it using the serial console.

The motherboard connectors labeled *S1* and *S2* are serial ports (UART) to which U-Boot and the Linux kernel output diagnostic information on startup. The correct serial port depends on your Processor Module:

=============== ==== =======
Module          Port Baud
=============== ==== =======
i.MX 8M Q       S1   115200
i.MX 8M Plus    S2   115200
RCM4-BPi/A311D  S2   115200
RCM4-RPi        S1   115200
RK3588          S1   1500000
=============== ==== =======

Wire up a 3.3V USB-to-UART adapter to 3 of the following pins of connector J17:

=== ===========================================
Pin Function
=== ===========================================
1   S2 UART2_TXD, connect to RX of your adapter
2   S2 UART2_RXD, connect to TX of your adapter
3   S2 GND, connect to GND of your adapter
4   S1 UART1_TXD, connect to RX of your adapter
5   S1 UART1_RXD, connect to TX of your adapter
6   S1 GND, connect to GND of your adapter
=== ===========================================

Then, use a terminal program such as ``tio`` on your host computer:

.. code-block:: none

   tio /dev/ttyUSB0 -b 115200 # Check Module manual
                              # for baud rate

If you then switch on Pocket Reform, you should see the U-Boot console in ``tio``. Please note that most kernel output is suppressed by the default kernel parameter ``loglevel=3``. Override this parameter if you need to see more output.

Electronics Repair
++++++++++++++++++

Before attempting an electronics fix, please contact our customer support at ``support@mntre.com``. Additionally, please consult the MNT Community forum (`<https://community.mnt.re>`_) for similar issues. Check our repair guides if you are knowledgeable with electronics. Open the respective KiCad file, for example the motherboard file, and take a look at the schematics and PCB layout. You can trace the power rails and measure them at different points on the board. You need a digital multimeter for this.

Software Issues
+++++++++++++++

If you suspect software issues, run the ``reform-check`` command (see "Software" chapter, "Reform Tools" section in this handbook).

System Boot
-----------

Unlike a PC that uses BIOS, Pocket Reform utilizes U-Boot, a bootloader commonly used on ARM processors. At the time of writing, Pocket Reform only supports ARM based modules.

The Processor Module will try to load boot code (U-Boot) from eMMC. If that fails, the standard i.MX8M Plus Module can only be booted using a special USB bootloader mode, which currently requires the Ezurio Nit8MP_ENC_CAR carrier board, a USB connection to another computer and the `uuu` tool from NXP. Please be careful when modifying the boot partition on eMMC because it is hard to recover without a carrier board. Other Processor Modules may have different boot device options---please refer to their specific documentation.

U-Boot is like a mini operating system and shell that allows you to inspect parts of the system (like PCIe, USB devices or Ethernet) and set up parameters to be passed to the real operating system kernel such as Linux, and start it. Every module has its own U-Boot version, specifically adapted for Pocket Reform. The sources and build instructions for all versions can be found at: `<https://source.mnt.re/reform>`_

U-Boot needs 3 files to boot Linux:

- The Linux kernel itself, named ``vmlinuz-...-reform2-arm64``.
- The device tree blob (DTB). The file has a Processor Module specific file name ending with ``.dtb``. The device tree is a data structure that lists the addresses of and parameters for all the devices in the system that Linux needs to initialize drivers for.
- U-Boot looks for a file called ``boot.scr`` on the boot medium, which is a script of commands that performs the actual loading of the OS. This script is normally managed by Debian and has a binary header. Usually, you never need to edit it yourself. If you really need to and know what you're doing, a tool called ``mkimage`` is required to update it.

Theoretically, you can boot other operating systems and Linux distributions besides Debian, such as FreeBSD, NetBSD, OpenBSD---if they include all necessary drivers for your Processor Module and provide an ``extlinux.conf`` file for U-Boot. Refer to `<https://docs.u-boot.org>`_ for details. Booting another Linux distribution can be achieved by combining the MNT Pocket Reform Linux kernel and DTB on the boot partition with the distribution's root file system extracted to the second partition.
