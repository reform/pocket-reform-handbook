#!/bin/bash

# extract content pages
pdftk pocket-reform-handbook.pdf cat 3-134 output pocket-reform-handbook-corr-content.pdf

# convert to outlines and remove comments etc
gs -dPreserveAnnots=false -dShowAnnots=false -dNoOutputFonts -o pocket-reform-handbook-corr-preflight.pdf -dAutoFilterColorImages=false -dAutoFilterGrayImages=false -dColorImageFilter=/FlateEncode -dGrayImageFilter=/FlateEncode -sDEVICE=pdfwrite pocket-reform-handbook-corr-content.pdf
