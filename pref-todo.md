# meeting notes 2024-05-30

chapter 7: hardware

- [x] Anri: low prio: 7.1.1 graphics mit padding
- [x] Anri: 7.1.2 aus saetzen stichpunkte machen (bei den bullet points)
- Lukas: chapter 7 (hardware) mit frames und covers nochmal die tiefe der PCBs checken
- [x] Anri: 7.2 motherboard render with callouts statt graphics, ports weglassen (sim card muss drin bleiben)
- Lukas: 7.2.3 (system controller) old info überarbeiten
- Lukas: 7.2.4 (flashing the firmware) komplett umschreiben
- Lukas: 7.26 und 7.2.7 neu schreiben
- [x] Anri: 7.4.1 in sätze fassen
- Lukas: 7.4.2 dependencies hinschreiben
- Lukas: bonus: 7.5 render kleiner machen :D
  - Lukas: link fixen
- [x] Anri: 7.7 Headphone Board text fehlt --> wi fi antenne ist on the empty area geklebt, power switch erwaehnen, headset jack erwaehnen
- [x] Anri: delete the standby section in linux chapter
- Lukas: top back cover rendering mit logo
- Lukas: advanced 8.1.1 boot issues umschreiben
- Lukas: advanced 8.1.2 serial console etwas ausdenken
- Lukas: advanced 8.1.4 system controller umschreiben
- Lukas: advanced 8.2 umschreiben
- Lukas: 9.11 (assembly parts) fehlt noch
- Lukas: Preface

- [x] das wo "UART and" hinzeigt ist eigentlich Internal USB-C of System Controller
- [x] das wo Speaker Connector draufzeigt ist Headset Connector
- [x] neben display connector links gibts noch nen kleinen unbeschrifteten Power Switch Connector
- Das wo Serial Ports (UARTs) drauf zeigt ist System Controller Programming Switch (etwas lang, kann auch Flashing sein)
- [x] diese große "gabel" ganz links ist Serial Ports (UARTs)
- [x] neben der gabel links ist "UART to Keyboard" 
- neben der gabel rechts ist "Speaker Connector"
- [x] neu: links neben M.2 for SSD ist ein kleiner connector, der ist Qwiic Connector

# Required Graphics

- [x] TODO hazards (pref bottom half von unten offen mit charger+batteries)
- [x] TODO standby power switch close-up render mit callout (es ist ein schiebeschalter, callout sagt in welche richtung on und off ist)

# Full Page Illustrations

- [x] TODO magic: nach quickstart / vor input devices
- [x] TODO sofa: vor linux basics
- [x] TODO cafe: vor graphical desktops
- [x] TODO warehouse: vor software
- [x] TODO network: vor hardware kapitel
- [x] TODO lab: vor advanced kapitel
- [x] TODO subway: am ende nach credits (lyric raus)

# Content

- [x] Safety/Hazards
  - [x] When fully charged... current kann weg
  - [x] Do not touch: anything connected to the batteries or charger board with metal tools. Refer to the Hardware chapter for instructions about safely exchanging batteries.
  - [x] New hazard: case is closing with strong magnets and the top half has sharp edges. Keep out of reach of children! Hazard of pinching fingers!

- [x] Quickstart: Step 1:
  - [x] "After reading... bottom plate" muss weg
  - [x] TODO standby power switch graphic rein (eine reicht)
- [x] Step 2: Turn On:
  - [x] TODO (anri) keyboard grafik muss neu
  - [x] "First, insert the... device" muss weg
  - [x] ... hold Hyper and Enter for more than 2 seconds.
- [x] Step 3: Log In:
  - [x] installed on the built-in eMMC flash. (statt "inserted SD card")

- [x] Input Devices:
  - [x] AGR key gibt es noch, also kurz erklären. Der ist v.a. dazu da international characters abzubilden.
  - [x] Können wir hier den Content vom Crowd Supply Blog Post zu EURkey reinholen? Inkl. vorhandene Grafik mit den Sonderzeichen.
  - [x] TODO: Hinweis im Setup Wizard: Es gibt nur das QWERTY-US physical layout bei Pocket Reform. Deswegen empfehlen wir für die meisten Leute in Europe das EURkey (eu) Layout zu wählen und Umlaute und Sonderzeichen mit AGR zu kriegen.
  - [x] Sätze mit ellipsis/user defined key raus, den gibt es ja nicht.

- [x] OLED Menu:
  - [x] TODO Stimmt wieder alles, weil OLED Menü wieder da ist. Nur die Grafik fehlt :3
  - [x] bei der ersten Grafik fehlt das reingelinkte PNG mit OLED-Pixelgrafik (gibts im Reform handbook irgendwo)
  - [ ] Bei battery information:
    - [x] verlinkte Grafik fehlt. TODO: Hier können wir das alte PNG nehmen und 6 Cells rauseditieren (Rücksprache dazu nochmal)
    - [x] Text: eight battery cells -> two battery cells
    - [x] Es gibt keine "Groups" mehr, text vereinfachen
    - [x] Oberes icon: linke battery cell (wenn device von unten betrachtet)
    - [x] Unteres icon: rechte battery cell (TODO: check ob korrekt)
    - [x] spinnenartige batterygrafik inkl text raus (ist schon raus, oder?)

- [x] Trackball:
  - [x] TODO Lukas: make trackball buttons like they are in the manual!
  - [x] "two middle buttons" stimmt nicht.
  - [x] hat auch keine "two scroll mode buttons". nur einen
  - [x] TODO: sticky scroll mode togglen per druck auf middle button + scroll mode button. -> hmm, hab ich vergessen zu machen in der firmware

- [x] Linux Console Basics:
  - [x] vorher .rst datei aus reform handbook drüberkopieren --> added to lukas' own todo
  - [x] "on the provided SD card" -> on the integrated eMMC flash.
  - [x] "With the SD card inserted" -> raus
  - [x] Dual Display:
    - [x] MNT Reform -> Pocket Reform
    - [x] Folgende Module raus: i.MX8MQ, LS1028A, Raspberry Pi
    - [x] "To activate dual display..." rausnehmen/umschreiben, so dass der Befehl+iMX8MQ nicht mehr drinne sind. -> will activate a second display automatically.

- [x] Graphical Desktops:
  - [x] sieht erstmal fine aus

- [x] Software:
  - [x] "In the first edition of the handbook..." weg, wir gehen davon aus dass MNT Reform nicht bekannt ist
  - [x] Text vereinfachen
  - [x] Flashing the System Image:
    - [x] "If your SD card is broken" -> If you want to reinstall your system
    - [x] Dann erklären, dass Pocket Reform versucht von einer MicroSD-Karte zu booten, wenn diese eingelegt ist.
    - [x] Wenn bisher von SD card die Rede ist, ersetzen durch MicroSD.
    - [x] Tabelle mit images:
      - [x] imx8mq, ls1028a raus
      - [x] neue filenames: pocket-reform-system...
    - [x] bei gunzip: pocket-reform-system-imx8mp.img.gz (wichtig: "p")
    - [x] analog bei weiteren commands
  - [x] Reform Tools:
    - [x] reform-display-config raus

- [ ] Hardware:
  - [ ] Case Parts:
    - [x] Main Box/Screen Back: Ersetzen durch Top Half und Bottom Half
    - [x] Top Back Cover
    - [x] Bottom Back Cover
    - [x] Display Frame (vormals top front plate)
    - [x] Keyboard Frame (vormals bottom front plate)
    - [x] TODO: Render Top Half
    - [x] TODO: Render Bottom Half
    - [x] TODO: Render Top Back Cover
    - [x] TODO: Render Bottom Back CCover
    - [x] TODO: Render Display Frame
    - [ ] TODO: Render Keyboard Frame
    - [x] Top Half:
      - [x] es fehlt noch beschreibung der cables intern
        - [x] a 2-pin JST-SH (4cm) cable connects the standby switch to the motherboard.
        - [x] a 5-pin JST-SH (4cm) cable connects the headset jack (headphone and microphone signals) to the motherboard.
        - [x] 2x 4-pin JST-SH (8cm) cables connect the keyboard module's USB and UART ports to the motherboard, through the hinges.
        - [x] 2x 4-pin JST-PH (22cm) cables connect the charger module's power port to the motherboard's 9-pin JST-PH power connector. This connector is split into a ground and a voltage cable.
        - [x] the display is connected to the motherboard with an FPC MIPI-DSI cable.
    - [x] check and edit Bottom Half
    - [x] Port covers:
      - [x] powder coated steel stimmt nicht, es sind PCBs
      - [x] M2x5 erstmal ersetzen durch M2 screws (länge gerade unbekannt)
  - [ ] Motherboard:
    - [x] TODO: Render Motherboard
    - [x] M2x4 ersetzen durch M2 (TODO länge unbekannt), und sind auch keine pan head, sondern countersunk.
    - [x] Power system raus, weil ist auf Charger board
    - [x] System controller: schreiben, dass es ein Raspberry Pi RP2040 chip ist.
    - [x] DSI to eDP bridge: raus, gibt es nicht
    - [x] USB 3.0 hub:
      - [x] TUSB8040 ist aber jetzt TUSB8041
      - [x] four USB ports, two internal and two external
      - [x] USB load switches... raus, gibt es nicht
    - [x] Soundchip:
      - [x] Ist ein TI TLV320AIC3100, kein Wolfson/Cirrus
      - [x] powers one speaker, not two
    - [x] mPCIe slot: raus
    - [x] M.2 slot: Ändern in "M.2 Key M slot:"
      - [x] can be fitted with an NVMe SSD (solid state disk). An SSD is already installed in Hyper models of MNT Pocket Reform.
    - [x] Neuen "M.2 Key B slot:" rein.
      - [x] hier kann ein cellular Modem eingesetzt werden.
      - [x] Wir empfehlen Quectel EM-06.
    - [x] ix Industrial:
      - [x] nehmen wir hier raus und packen es zu Ports (neuer Abschnitt)
    - [x] Neuer Punkt: "Internal USB-C Port"
      - [x] this port is for flashing the system controller wenn der system controller bricked ist und es nicht vom Processor Module aus geht. Dazu müssen 2 Jumper auf dem Header J21 umgesteckt werden (siehe Advanced Topics).
    - [x] Neuer Punkt: "Internal Qwiic Connector"
      - [x] This 4-pin JST-SH connector offers I2C signals and 3.3V power compatible with Sparkfun's Qwiic sensors, displays etc. See "Advanced Topics" for more details.
    - [x] Neuer Abschnitt "Right Side Ports":
      - [x] 2x USB-C:
        - [x] Einer der USB-C Ports hat USB-C Power Delivery (PD) functionality, darüber kann das Gerät geladen werden. (oberster oder äußerster Port -> in Grafik ersichtlich).
        - [x] Beide Ports unterstützen den Anschluss von USB3.0/2.0/1.0 Geräten.
        - [x] Es gibt keine Alternate Modes, also kann eins kein DisplayPort Display o.ä. anschließen.
      - [x] MicroSD:
        - [x] kann eins eine MicroSD-Karte reinstecken
        - [x] Achtung: System kann von der Karte gebooted werden, wenn ein entsprechendes Image drauf ist.
      - [x] Micro HDMI:
        - [x] hier kann mittels Adapter ein HDMI Display angeschlossen werden. Es ist ein Output, kein Input. Nicht zu verwechseln mit Mini HDMI beim Adapterkauf.
      - [x] ix Industrial:
        - [x] hier rein was vorher beim Motherboard direkt stand (aber transformer chip raus).
    - [x] "Left Side Ports":
      - [x] Headset Jack: siehe port covers, text hier rüberholen
      - [x] Standby Power Switch: siehe port covers und quick start

RENDERS - callout list
- [x] headphone-board-x2:
  - [x] Headset jack
  - [x] Audio connector to motherboard
  - [x] Standby power switch
  - [x] Power connector to motherboard
- [x] panel-display-bezel:
  - [x] Speaker grille
- [x] trackball2:
  - [x] 15mm POM (acetal) ball
  - [x] 2.5mm steel beads
  - [x] Optical sensor
  - [x] FPC connector to motherboard
- [ ] keyboard:
  - [x] da muss ich dir zeigen wo die sachen sind
  - [x] RP2040 microcontroller
  - [x] Kailh choc keyswitches
  - [x] Custom MNT Pocket keycaps by FKcaps
  - [x] Internal USB connector to motherboard
  - [x] Internal UART connector to motherboard
  - [x] FPC connector to OLED module
  - Trackball mounting zone (evtl obvious --> yes)
- [x] cpu module-pads:
  - [x] i.MX8M Plus Processor
  - [x] 8GB LPDDR4 Memory
  - [x] 128GB eMMC Flash
  - [x] QCA9377 WiFi/BT chipset
  - [x] WiFi/BT antenna connector
  - [x] MIPI-DSI display connector to motherboard
- [x] charger:
  - [x] (hier fehlt ein chip im model aaaaah, vllt egal)
  - [x] Split power connector to motherboard
  - [x] Thermal sensors
  - [x] Battery cell connectors
